# aaseat

aaseat is a SeAT-like plugin for [Alliance Auth](https://gitlab.com/allianceauth/allianceauth).

## Installation

With your auth venv active, install the django app with the following command `pip install git+https://gitlab.com/colcrunch/aaseat`.

### The Settings File
In your local file, add `'aaseat',` to the `INSTALLED_APPS` section of `local.py`.

You will also need to add the following to the end of your `local.py` file in order to keep the names from the SDE up to date. 
```python
CELERYBEAT_SCHEDULE['run_sde_names_update'] = {
    'task': 'aaseat.tasks.update_names_from_sde',
    'schedule': crontab(day_of_month='1,15'),
}
```

### The Next Step
Once you have the settings file set up, make sure to run the migrations with `python manage.py migrate`, then restart your auth related processes. (Likely just `sudo supervisorctl restart myauth:*` for most)

Then go to your django-admin page and navigate to the *Periodic Tasks* section. Click the check box next to `run_sde_names_update`, and in the dropdown at the top of the page select *Run Selected Tasks*. This will populate the names from the SDE for the first time.


## Issues

Please remember to report any aaseat related issues using the issues on **this** repository.

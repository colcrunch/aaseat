# -*- coding: utf-8 -*-
from setuptools import setup, find_packages
from aaseat import __version__

install_requires = [
    'django>=2.0,<2.1',
    'django-bootstrap-form',
    'allianceauth>=2.1.1',
    'django-model_utils>=3.1.1'
]

testing_extras = [

]

setup(
    name='aaseat',
    version=__version__,
    author='Col Crunch',
    author_email='it-team@serin.space',
    description='A SeAT like plugin for use with Alliance Auth',
    install_requires=install_requires,
    extras_require={
        'testing': testing_extras,
        ':python_version=="3.4"': ['typing'],
    },
    python_requires='~=3.4',
    license='GPLv3',
    packages=find_packages(),
    url='https://gitlab.com/colcrunch/aaseat',
    zip_safe=False,
    include_package_data=True,
)

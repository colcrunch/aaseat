import logging

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.contrib import messages
from esi.decorators import token_required
from allianceauth.eveonline.models import EveCharacter
from django.db import IntegrityError

from .models import SeatCharacter
from .tasks import update_character

CORP_REQUIRED_SCOPES = [
    'esi-characters.read_corporation_roles.v1',
    'esi-universe.read_structures.v1',
    'esi-assets.read_corporation_assets.v1',
    'esi-corporations.read_standings.v1',
    'esi-contracts.read_corporation_contracts.v1',
    'esi-industry.read_corporation_mining.v1',
    'esi-industry.read_corporation_jobs.v1',
    'esi-corporations.read_structures.v1',
    'esi-wallet.read_corporation_wallets.v1',
    'esi-corporations.read_divisions.v1',
]

CHAR_REQUIRED_SCOPES = [
    'esi-calendar.read_calendar_events.v1',
    'esi-characters.read_notifications.v1',
    'esi-universe.read_structures.v1',
    'esi-fittings.read_fittings.v1',
    'esi-mail.read_mail.v1',
    'esi-industry.read_character_jobs.v1',
    'esi-industry.read_character_mining.v1',
    'esi-contracts.read_character_contracts.v1',
    'esi-characters.read_standings.v1',
    'esi-assets.read_assets.v1',
    'esi-clones.read_clones.v1',
    'esi-clones.read_implants.v1',
    'esi-wallet.read_character_wallet.v1',
    'esi-skills.read_skills.v1',
    'esi-location.read_location.v1',
    'esi-location.read_ship_type.v1',
    'esi-markets.read_character_orders.v1',
    'esi-skills.read_skillqueue.v1',
]


@login_required
#@permission_required('aaseat.access_aaseat')
def aaseat_dashboard(request):
    main_char = request.user.profile.main_character
    
    try:
        main_character = SeatCharacter.objects.get(character=main_char)
        alt_list = [co.character for co in main_char.character_ownership.user.character_ownerships.all()]
        alts = []
        for alt in alt_list:
            if not main_char == alt:
                try:
                    alts.append(SeatCharacter.objects.get(character=alt))
                except:
                    alt_item = {}
                    alt_item['character'] = alt
                    alts.append(alt_item)
                
        context = {
            'main_character': main_character,
            'alts': alts,
        }
        return render(request, 'aaseat/dashboard.html', context=context)
            
    except:
        logging.exception("message")
        
    return render(request, 'aaseat/dashboard.html')


@login_required
@token_required(scopes=CHAR_REQUIRED_SCOPES)
#@permission_required('aaseat.access_aaseat')
def aaseat_add_char(request, token):
    try:
        update_character(token.character_id)
        return redirect('aaseat:dashboard')
        
    except:
        messages.error(request, ('Error Adding Character!'))
        
    return redirect('aaseat:dashboard')

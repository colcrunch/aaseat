from django.db import models
from model_utils import Choices
from allianceauth.eveonline.models import EveCharacter, EveCorporationInfo
from allianceauth.authentication.models import User


# Create your models here.

# Implant Model
class Implant(models.Model):
    implant_id = models.IntegerField()


# Extend EveCharacter model
class SeatCharacter(models.Model):
    character = models.ForeignKey(EveCharacter, on_delete=models.CASCADE)
    total_sp = models.BigIntegerField(null=False, default=0)
    unallocated_sp = models.BigIntegerField(null=True, default=None)
    wallet_balance = models.DecimalField(max_digits=20, decimal_places=2)
    implants = models.ManyToManyField(Implant)
    last_location = models.BigIntegerField(null=True, default=None)
    last_ship_type = models.IntegerField(null=True, default=None)
    last_ship_name = models.CharField(max_length=255, null=True, default=None)
    sec_status = models.DecimalField(max_digits=6, decimal_places=2, default=0.0)

    class Meta:
        permissions = (('access_aaseat', 'Can access aaseat'),)

    def __str__(self):
        return "{}'s SeatCharacter".format(self.character.character_name)


# Note Models
class Note(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='%(class)s_author')
    content = models.TextField(max_length=8000)

    class Meta:
        abstract = True


class CharacterNote(Note):
    character = models.ForeignKey(SeatCharacter, on_delete=models.CASCADE)

    def __str__(self):
        return "{}'s Notes About {}".format(self.author, self.character)


class CorporationNote(Note):
    corporation = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return "{}'s Notes About {}".format(self.author, self.corporation)


# Asset Models
class Asset(models.Model):
    blueprint_copy = models.NullBooleanField(default=None)
    singleton = models.BooleanField()
    item_id = models.BigIntegerField(unique=True)
    location_flag = models.CharField(max_length=50)
    location_id = models.BigIntegerField()
    location_type = models.CharField(max_length=25)
    quantity = models.IntegerField()
    type_id = models.IntegerField()
    name = models.CharField(max_length=255, null=True, default=None)

    @property
    def is_bpc(self):
        return self.blueprint_copy

    @property
    def is_singleton(self):
        return self.singleton

    class Meta:
        abstract = True


class CorpAsset(Asset):
    corp = models.ForeignKey(EveCorporationInfo, on_delete=models.CASCADE)

    def __str__(self):
        return '{2} {0}x{1} ({3} / {4})'.format(self.type_id, self.quantity, self.corp,
                                                self.location_id, self.location_type)


class CharacterAsset(Asset):
    character = models.ForeignKey(SeatCharacter, on_delete=models.CASCADE)

    def __str__(self):
        return '{2} {0}x{1} ({3} / {4})'.format(self.type_id, self.quantity, self.character,
                                                self.location_id, self.location_type)


# Skills Model
class Skill(models.Model):
    character = models.ForeignKey(SeatCharacter, on_delete=models.CASCADE)
    skill_id = models.IntegerField()
    active_skill_level = models.IntegerField()
    skillpoints_in_skill = models.BigIntegerField()
    trained_skill_level = models.IntegerField()


# Skill Queue Model
class SkillQueue(models.Model):
    character = models.ForeignKey(SeatCharacter, on_delete=models.CASCADE)
    # Required Fields / Fields Always Present
    finish_level = models.IntegerField()
    queue_position = models.IntegerField()
    skill_id = models.IntegerField()

    # Fields that may or may not be present
    finish_date = models.DateTimeField(null=True, default=None)
    level_end_sp = models.IntegerField(null=True, default=None)
    level_start_sp = models.IntegerField(null=True, default=None)
    start_date = models.DateTimeField(null=True, default=None)
    training_start_sp = models.IntegerField(null=True, default=None)


# Standings Model
class Standing(models.Model):
    character = models.ForeignKey(SeatCharacter, on_delete=models.CASCADE)
    from_id = models.BigIntegerField()
    from_type = models.CharField(max_length=10, null=False)
    standing = models.DecimalField(max_digits=4, decimal_places=2)


# Contacts Models
class Contact(models.Model):
    contact_id = models.BigIntegerField()
    contact_type = models.CharField(max_length=255, null=False)
    standing = models.DecimalField(max_digits=4, decimal_places=2)

    class Meta:
        abstract = True


class ContactLabel(models.Model):
    label_id = models.BigIntegerField(primary_key=True)
    label_name = models.CharField(max_length=255, null=False)

    class Meta:
        abstract = True


class CharacterContactLabel(ContactLabel):
    character = models.ForeignKey(SeatCharacter, on_delete=models.CASCADE)


class CorporationContactLabel(ContactLabel):
    corporation = models.ForeignKey(EveCorporationInfo, on_delete=models.CASCADE)


class CharacterContact(Contact):
    character = models.ForeignKey(SeatCharacter, on_delete=models.CASCADE)
    blocked = models.NullBooleanField()
    watched = models.NullBooleanField()
    labels = models.ManyToManyField(CharacterContactLabel)

    @property
    def is_watched(self):
        if self.watched:
            return True
        else:
            return False

    @property
    def is_blocked(self):
        if self.blocked:
            return True
        else:
            return False


class CorporationContact(Contact):
    corporation = models.ForeignKey(EveCorporationInfo, on_delete=models.CASCADE)
    watched = models.NullBooleanField()
    labels = models.ManyToManyField(CorporationContactLabel)

    @property
    def is_watched(self):
        if self.watched:
            return True
        else:
            return False


# Character Mining Ledger Model
class CharacterMiningLedger(models.Model):
    character = models.ForeignKey(SeatCharacter, on_delete=models.CASCADE)
    date = models.DateTimeField()
    type_id = models.IntegerField()
    quantity = models.BigIntegerField()
    solar_system_id = models.IntegerField()


# Indy Job Model
class IndustryJob(models.Model):
    # ID
    job_id = models.BigIntegerField(unique=True)
    
    # Job Stats
    activity_id = models.IntegerField()
    _status_enum = Choices('active', 'cancelled',
                           'delivered', 'paused', 'ready', 'reverted')
    status = models.CharField(max_length=10, choices=_status_enum, null=False)
    probability = models.DecimalField(max_digits=5, decimal_places=2)
    runs = models.IntegerField()
    successful_runs = models.IntegerField(null=True, default=None)
    cost = models.DecimalField(max_digits=20, decimal_places=2, null=True, default=None)
    duration = models.IntegerField()  # in seconds

    # BP details
    blueprint_id = models.BigIntegerField()
    blueprint_type_id = models.IntegerField()
    product_type_id = models.BigIntegerField(null=True, default=None)
    licensed_runs = models.IntegerField(null=True, default=None)

    # dates
    start_date = models.DateTimeField()
    pause_date = models.DateTimeField(null=True, default=None)
    end_date = models.DateTimeField()
    completed_date = models.DateTimeField(null=True, default=None)

    # who
    installer_id = models.BigIntegerField()
    completed_character_id = models.IntegerField(null=True, default=None)

    # locations
    blueprint_location_id = models.BigIntegerField()  # could be an asset
    facility_id = models.BigIntegerField()  # could be a structure or SDE data
    output_location_id = models.BigIntegerField()  # could be an asset
    station_id = models.BigIntegerField()  # could be a structure or SDE data

    class Meta:
        abstract = True


class CorpIndustryJob(IndustryJob):
    corp = models.ForeignKey(EveCorporationInfo, on_delete=models.CASCADE)

    def __str__(self):
        return '{0} {1} ({2} / {3})'.format(self.job_id, self.corp,
                                            self.output_location_id, self.facility_id)


class CharacterIndustryJob(IndustryJob):
    character = models.ForeignKey(SeatCharacter, on_delete=models.CASCADE)

    def __str__(self):
        return '{0} {1} ({2} / {3})'.format(self.job_id, self.character,
                                            self.output_location_id, self.facility_id)


class MailLabel(models.Model):
    character = models.ForeignKey(SeatCharacter, on_delete=models.CASCADE)
    label_id = models.IntegerField(default=None)
    label_name = models.CharField(max_length=255, null=True, default=None)
    _color_enum = Choices('#0000fe', '#006634', '#0099ff', '#00ff33', '#01ffff',
                          '#349800', '#660066', '#666666', '#999999', '#99ffff',
                          '#9a0000', '#ccff9a', '#e6e6e6', '#fe0000', '#ff6600',
                          '#ffff01', '#ffffcd', '#ffffff')
    label_color = models.CharField(max_length=7, choices=_color_enum, null=True, default=None)
    unread_count = models.IntegerField(null=True, default=None)


class MailRecipient(models.Model):
    recipient_id = models.BigIntegerField(primary_key=True, unique=True)
    _recipient_enum = Choices('alliance', 'character', 'corporation', 'mailing_list')
    recipient_type = models.CharField(max_length=15, choices=_recipient_enum)


# Mail Message
class MailMessage(models.Model):
    character = models.ForeignKey(SeatCharacter, on_delete=models.CASCADE)
    id_key = models.BigIntegerField(primary_key=True, unique=True)

    # headers
    mail_id = models.IntegerField(null=True, default=None)
    from_id = models.IntegerField(null=True, default=None)
    recipients = models.ManyToManyField(MailRecipient)
    labels = models.ManyToManyField(MailLabel)
    is_read = models.NullBooleanField(default=None)
    timestamp = models.DateTimeField(null=True, default=None)

    # message
    subject = models.CharField(max_length=255, null=True, default=None)
    body = models.CharField(max_length=12000, null=True, default=None)


# Fitting
class Fitting(models.Model):
    character = models.ForeignKey(SeatCharacter, on_delete=models.CASCADE)

    description = models.CharField(max_length=1000)  # unsure on max length here
    fitting_id = models.IntegerField()
    name = models.CharField(max_length=255, null=False)
    ship_type_id = models.IntegerField()


# Fitting items
class FittingItem(models.Model):
    fit = models.ForeignKey(Fitting, on_delete=models.CASCADE)

    flag = models.IntegerField()
    quantity = models.IntegerField()
    type_id = models.IntegerField()


# Corp mining observers
class MiningObserver(models.Model):
    corporation = models.ForeignKey(EveCorporationInfo, on_delete=models.CASCADE)

    last_updated = models.DateTimeField()
    observer_id = models.BigIntegerField()
    _observer_enum = Choices('structure')
    observer_type = models.CharField(max_length=10, choices=_observer_enum)


# Corp Mining Observation
class MiningObservation(models.Model):
    observer = models.ForeignKey(MiningObserver, on_delete=models.CASCADE)

    character_id = models.IntegerField()
    last_updated = models.DateTimeField()
    quantity = models.BigIntegerField()
    recorded_corporation_id = models.IntegerField()
    type_id = models.IntegerField()


# Structure
class Structure(models.Model):
    corporation = models.ForeignKey(EveCorporationInfo, on_delete=models.CASCADE)

    fuel_expires = models.DateTimeField(null=True, default=None)
    next_reinforce_apply = models.DateTimeField(null=True, default=None)
    next_reinforce_weekday = models.IntegerField(null=True, default=None)
    profile_id = models.IntegerField()
    reinforce_hour = models.IntegerField()
    reinforce_weekday = models.IntegerField(null=True, default=None)
    _state_enum = Choices('anchor_vulnerable', 'anchoring', 'armor_reinforce', 'armor_vulnerable',
                          'fitting_invulnerable', 'hull_reinforce', 'hull_vulnerable', 'online_deprecated',
                          'onlining_vulnerable', 'shield_vulnerable', 'unanchored', 'unknown')
    state = models.CharField(max_length=25, choices=_state_enum)
    state_timer_end = models.DateTimeField(null=True, default=None)
    state_timer_start = models.DateTimeField(null=True, default=None)
    structure_id = models.BigIntegerField()
    system_id = models.IntegerField()
    type_id = models.IntegerField()
    unanchors_at = models.DateTimeField(null=True, default=None)

    # extras
    name = models.CharField(max_length=200, null=True, default=None)


# Structure service
class StructureService(models.Model):
    structure = models.ForeignKey(Structure, on_delete=models.CASCADE)

    name = models.CharField(max_length=100)
    _state_enum = Choices('online', 'offline', 'cleanup')
    state = models.CharField(max_length=8, choices=_state_enum)


# Corporation history
class CorpHistory(models.Model):
    character = models.ForeignKey(SeatCharacter, on_delete=models.CASCADE)

    corporation_id = models.IntegerField()
    is_deleted = models.NullBooleanField(null=True, default=None)
    record_id = models.IntegerField()
    start_date = models.DateTimeField()


# Notifications
class Notification(models.Model):
    character = models.ForeignKey(SeatCharacter, on_delete=models.CASCADE)

    notification_id = models.BigIntegerField()
    sender_id = models.IntegerField()
    _type_enum = Choices('character', 'corporation', 'alliance', 'faction', 'other')
    sender_type = models.CharField(max_length=15, choices=_type_enum)
    notification_text = models.TextField(null=True, default=None)
    timestamp = models.DateTimeField()
    notification_type = models.CharField(max_length=50)
    is_read = models.NullBooleanField(null=True, default=None)


# Wallet Models
class WalletJournalEntry(models.Model):
    amount = models.DecimalField(max_digits=20, decimal_places=2, null=True, default=None)
    balance = models.DecimalField(max_digits=20, decimal_places=2, null=True, default=None)
    context_id = models.BigIntegerField(null=True, default=None)
    _context_type_enum = Choices('structure_id', 'station_id', 'market_transaction_id', 'character_id',
                                 'corporation_id', 'alliance_id', 'eve_system', 'industry_job_id',
                                 'contract_id', 'planet_id', 'system_id', 'type_id')
    context_id_type = models.CharField(max_length=30, choices=_context_type_enum, null=True, default=None)
    date = models.DateTimeField()
    description = models.CharField(max_length=500)
    first_party_id = models.IntegerField(null=True, default=None)
    entry_id = models.BigIntegerField()
    reason = models.CharField(max_length=500, null=True, default=None)
    ref_type = models.CharField(max_length=72)
    second_party_id = models.IntegerField(null=True, default=None)
    tax = models.DecimalField(max_digits=20, decimal_places=2, null=True, default=None)
    tax_reciever_id = models.IntegerField(null=True, default=None)

    class Meta:
        abstract = True


class CorporationWalletDivision(models.Model):
    corporation = models.ForeignKey(EveCorporationInfo, on_delete=models.CASCADE)

    balance = models.DecimalField(max_digits=20, decimal_places=2)
    division = models.IntegerField()


class CharacterWalletJournalEntry(WalletJournalEntry):
    character = models.ForeignKey(SeatCharacter, on_delete=models.CASCADE)


class CorporationWalletJournalEntry(WalletJournalEntry):
    division = models.ForeignKey(CorporationWalletDivision, on_delete=models.CASCADE)


class WalletTransactionEntry(models.Model):
    client_id = models.IntegerField()
    date = models.DateTimeField()
    is_buy = models.BooleanField()
    journal_ref_id = models.BigIntegerField()
    location_id = models.BigIntegerField()
    quantity = models.BigIntegerField()  # Only requires IntegerField, but this is to be safe.
    transaction_id = models.BigIntegerField()
    type_id = models.IntegerField()
    unit_price = models.DecimalField(max_digits=20, decimal_places=2)


class CharacterWalletTransactionEntry(WalletTransactionEntry):
    character = models.ForeignKey(SeatCharacter, on_delete=models.CASCADE)
    is_personal = models.BooleanField()


class CorporationWalletTransactionEntry(WalletTransactionEntry):
    division = models.ForeignKey(CorporationWalletDivision, on_delete=models.CASCADE)


# Contract
class Contract(models.Model):
    acceptor_id = models.IntegerField()
    assignee_id = models.IntegerField()
    _avail_enum = Choices('public', 'personal', 'corporation', 'alliance')
    availability = models.CharField(max_length=11, choices=_avail_enum)
    buyout = models.DecimalField(max_digits=20, decimal_places=2, null=True, default=None)
    collateral = models.DecimalField(max_digits=20, decimal_places=2, null=True, default=None)
    contract_id = models.IntegerField()
    date_accepted = models.DateTimeField(null=True, default=None)
    date_completed = models.DateTimeField(null=True, default=None)
    date_expired = models.DateTimeField()
    days_to_complete = models.IntegerField(null=True, default=None)
    end_location_id = models.BigIntegerField(null=True, default=None)
    for_corporation = models.BooleanField()
    issuer_corporation_id = models.IntegerField()
    issuer_id = models.IntegerField()
    price = models.DecimalField(max_digits=20, decimal_places=2)
    reward = models.DecimalField(max_digits=20, decimal_places=2)
    start_location_id = models.BigIntegerField(null=True, default=None)
    _status_enum = Choices('outstanding', 'in_progress', 'finished_issuer', 'finished_contractor',
                           'finished', 'cancelled', 'rejected', 'failed', 'deleted', 'reversed')
    status = models.CharField(max_length=11, choices=_status_enum)
    title = models.CharField(max_length=255)
    _type_enum = Choices('unknown', 'item_exchange', 'auction', 'courier', 'loan')
    contract_type = models.CharField(max_length=18, choices=_type_enum)
    volume = models.DecimalField(max_digits=30, decimal_places=2)

    class Meta:
        abstract = True


class CharacterContract(Contract):
    character = models.ForeignKey(SeatCharacter, on_delete=models.CASCADE)


class CorporationContract(Contract):
    corporation = models.ForeignKey(EveCorporationInfo, on_delete=models.CASCADE)


class ContractItem(models.Model):
    included = models.BooleanField()
    singleton = models.BooleanField()
    quantity = models.IntegerField()
    raw_quantity = models.IntegerField(null=True, default=None)
    record_id = models.BigIntegerField()
    type_id = models.IntegerField()

    class Meta:
        abstract = True


class CorporationContractItem(ContractItem):
    contract = models.ForeignKey(CorporationContract, on_delete=models.CASCADE)


class CharacterContractItem(ContractItem):
    contract = models.ForeignKey(CharacterContract, on_delete=models.CASCADE)


# Market Models
class MarketOrder(models.Model):
    duration = models.IntegerField()
    escrow = models.IntegerField(null=True, default=None)
    is_buy_order = models.NullBooleanField(null=True, default=None)
    issued = models.DateTimeField()
    location_id = models.BigIntegerField()
    min_volume = models.IntegerField(null=True, default=None)
    order_id = models.BigIntegerField()
    price = models.DecimalField(max_digits=20, decimal_places=2, null=True, default=None)
    _range_enum = Choices('1', '10', '2', '20', '3', '30', '4', '40', '5', 'region', 'solarsystem', 'station')
    range = models.CharField(max_length=30, choices=_range_enum)
    region_id = models.IntegerField()
    type_id = models.IntegerField()
    volume_remain = models.IntegerField()
    volume_total = models.IntegerField()

    class Meta:
        abstract = True


class CorporationMarketOrder(MarketOrder):
    wallet_division = models.ForeignKey(CorporationWalletDivision, on_delete=models.CASCADE)
    issued_by = models.IntegerField()


class CharacterMarketOrder(MarketOrder):
    character = models.ForeignKey(SeatCharacter, on_delete=models.CASCADE)
    is_corporation = models.BooleanField()


class CorporationMarketOrderHistory(MarketOrder):
    wallet_division = models.ForeignKey(CorporationWalletDivision, on_delete=models.CASCADE)
    issued_by = models.IntegerField()
    _state_enum = Choices('cancelled', 'expired')
    state = models.CharField(max_length=30, choices=_state_enum)


class CharacterMarketOrderHistory(MarketOrder):
    character = models.ForeignKey(SeatCharacter, on_delete=models.CASCADE)
    is_corporation = models.BooleanField()
    _state_enum = Choices('cancelled', 'expired')
    state = models.CharField(max_length=30, choices=_state_enum)


# Calendar
class CalendarEvent(models.Model):
    character = models.ForeignKey(SeatCharacter, on_delete=models.CASCADE)
    date = models.DateTimeField()
    duration = models.IntegerField()
    event_id = models.IntegerField()
    importance = models.IntegerField()
    owner_id = models.IntegerField()
    owner_name = models.CharField(max_length=50)
    _owner_type_enum = Choices('eve_server', 'corporation', 'faction', 'character', 'alliance')
    owner_type = models.CharField(max_length=15, choices=_owner_type_enum)
    response = models.CharField(max_length=500)  # Length unknown
    text = models.CharField(max_length=5000)  # Length unknown
    title = models.CharField(max_length=500)  # Length unknown


# Jump Clone Models
class JumpClone(models.Model):
    character = models.ForeignKey(SeatCharacter, on_delete=models.CASCADE)

    implants = models.ManyToManyField(Implant)
    jump_clone_id = models.IntegerField()
    location_id = models.BigIntegerField()
    _type_enum = Choices('station', 'structure')
    location_type = models.CharField(max_length=9, choices=_type_enum)
    name = models.CharField(max_length=255, null=True, default=None)


# Name Class
class ItemName(models.Model):
    name = models.CharField(max_length=500)
    item_id = models.BigIntegerField(primary_key=True)


class TypeName(models.Model):
    name = models.CharField(max_length=500)
    type_id = models.BigIntegerField(primary_key=True)


class EveName(models.Model):
    name = models.CharField(max_length=500)
    name_id = models.IntegerField()
    _category_enum = Choices('alliance', 'character',
                             'constellation', 'corporation',
                             'inventory_type', 'region',
                             'solar_system', 'station', 'faction')
    category = models.CharField(max_length=30, choices=_category_enum)

from django.contrib import admin

# Register your models here.
from .models import SeatCharacter

admin.site.register(SeatCharacter)

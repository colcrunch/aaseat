from django.template.defaulttags import register
from ..models import ItemName, TypeName


@register.filter
def item(item_id):
    try:
        name = ItemName.objects.get(item_id=item_id).name
        return name
    except:
        return item_id


@register.filter
def type_(type_id):
    try:
        name = TypeName.objects.get(type_id=type_id).name
        return name
    except:
        return type_id

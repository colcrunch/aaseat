from . import urls
from allianceauth import hooks
from allianceauth.services.hooks import MenuItemHook, UrlHook


class MoonMenu(MenuItemHook):
    def __init__(self):
        MenuItemHook.__init__(self, 'SeAT',
                              'fa fa-empire fa-fw',
                              'aaseat:dashboard',
                              navactive=['aaseat:'])

    def render(self, request):
        if request.user.has_perm('aaseat.access_aaseat'):
            return MenuItemHook.render(self, request)
        return ''


@hooks.register('menu_item_hook')
def register_menu():
    return MoonMenu()


@hooks.register('url_hook')
def register_url():
    return UrlHook(urls, 'aaseat', r'^seat/')

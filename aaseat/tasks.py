import logging
from celery import shared_task
from .models import CharacterWalletJournalEntry, CorpHistory, Notification, SeatCharacter, Skill, \
    CharacterMarketOrder, CharacterContactLabel, CharacterContact, Fitting, FittingItem, CharacterMiningLedger, \
    Standing, CalendarEvent, CharacterAsset, ItemName, TypeName, JumpClone, Implant, CharacterIndustryJob, MailLabel, \
    MailRecipient, MailMessage, CorpAsset, CorporationWalletDivision, CorporationWalletJournalEntry, Structure, \
    StructureService, CharacterContractItem, CharacterContract, CharacterWalletTransactionEntry,\
    CorporationWalletTransactionEntry, MiningObservation, MiningObserver, SkillQueue, EveName, CorporationContact, \
    CorporationContract
from allianceauth.eveonline.models import EveCharacter, EveCorporationInfo
from esi.models import Token
from esi.clients import esi_client_factory
from .esi_workaround import EsiResponseClient
from django.utils import timezone
from django.db.models import Q

import bz2
import re
import requests
import datetime

logger = logging.getLogger(__name__)


def _get_token(character_id, scopes):
    return Token.objects.filter(character_id=character_id).require_scopes(scopes)[0]


@shared_task
def update_character(character_id):
    logger.debug("Updating Character: %s" % str(character_id))

    req_scopes = ['esi-skills.read_skills.v1',
                  'esi-wallet.read_character_wallet.v1',
                  'esi-location.read_location.v1',
                  'esi-location.read_ship_type.v1',
                  ]

    token = _get_token(character_id, req_scopes)
    c = token.get_esi_client()

    character_data = c.Character.get_characters_character_id(character_id=token.character_id).result()
    wallet_ball = c.Wallet.get_characters_character_id_wallet(character_id=token.character_id).result()
    location_data = c.Location.get_characters_character_id_location(character_id=token.character_id).result()
    ship_data = c.Location.get_characters_character_id_ship(character_id=token.character_id).result()
    char = EveCharacter.objects.get(character_id=token.character_id)

    seat_char, created = SeatCharacter.objects.update_or_create(character=char, defaults={
        'wallet_balance': wallet_ball,
        'last_location': location_data.get('solar_system_id', 0),
        'last_ship_type': ship_data.get('ship_type_id', 0),
        'last_ship_name': ship_data.get('ship_name', "Unknown"),
        'sec_status': character_data.get('security_status', 0.0)})

    if created:  # fire some once off tasks this is a new character!
        logger.debug("New character firing full set of update tasks")
        update_character_corp_history.delay(token.character_id)
        update_character_skills.delay(token.character_id)
        update_character_notifications.delay(token.character_id)
        update_character_wallet_journal.delay(token.character_id)
        update_character_market_orders.delay(token.character_id)
        update_character_contacts.delay(token.character_id)
        update_character_fittings.delay(token.character_id)
        update_character_mining.delay(token.character_id)
        update_character_standings.delay(token.character_id)
        update_character_calendar_events.delay(token.character_id)
        update_character_clones.delay(token.character_id)
        update_character_industry_jobs.delay(token.character_id)
        update_character_assets.delay(token.character_id)
        update_character_mail.delay(token.character_id)


@shared_task
def update_character_corp_history(character_id):
    logger.debug("updating corp history for: %s" % str(character_id))

    req_scopes = ['publicData']

    token = _get_token(character_id, req_scopes)
    c = token.get_esi_client()
    seat_char = SeatCharacter.objects.get(character__character_id=character_id)

    corp_history = c.Character.get_characters_character_id_corporationhistory(character_id=character_id).result()
    for corp in corp_history:
        history_item, created = CorpHistory.objects \
            .update_or_create(character=seat_char,
                              record_id=corp.get('record_id'),
                              defaults={'corporation_id': corp.get('corporation_id'),
                                        'is_deleted': corp.get('is_deleted', False),
                                        'start_date': corp.get('start_date')})


@shared_task
def update_character_skills(character_id):
    logger.debug("updating skills for: %s" % str(character_id))

    req_scopes = ['esi-skills.read_skills.v1', 'esi-skills.read_skillqueue.v1']

    token = _get_token(character_id, req_scopes)
    c = token.get_esi_client()
    seat_char = SeatCharacter.objects.get(character__character_id=character_id)

    skills = c.Skills.get_characters_character_id_skills(character_id=character_id).result()

    # Update SeatCharacter model with correct SP values.
    seat_char.total_sp = skills.get('total_sp', 0)
    seat_char.unallocated_sp = skills.get('unallocated_sp', 0)
    seat_char.save()

    for skill in skills.get('skills', []):
        skill_item, created = Skill.objects \
            .update_or_create(character=seat_char, skill_id=skill.get('skill_id'),
                              defaults={
                                  'active_skill_level': skill.get('active_skill_level'),
                                  'skillpoints_in_skill': skill.get('skillpoints_in_skill'),
                                  'trained_skill_level': skill.get('trained_skill_level')})

    # ----- Skill Queue -----
    SkillQueue.objects.filter(character=seat_char).delete()  # Delete current SkillQueue

    queue = c.Skills.get_characters_character_id_skillqueue(character_id=character_id).result()

    items = []
    for item in queue:
        queue_item = SkillQueue(character=seat_char,
                                finish_level=item.get('finished_level'), queue_position=item.get('queue_position'),
                                skill_id=item.get('skill_id'), finish_date=item.get('finish_date', None),
                                level_end_sp=item.get('level_end_sp', None),
                                level_start_sp=item.get('level_start_sp', None),
                                start_date=item.get('start_date', None),
                                training_start_sp=item.get('training_start_sp', None))
        items.append(queue_item)
    SkillQueue.objects.bulk_create(items)


@shared_task
def update_character_notifications(character_id):
    logger.debug("Started notifications for: %s" % str(character_id))

    req_scopes = ['esi-characters.read_notifications.v1']

    token = _get_token(character_id, req_scopes)
    c = EsiResponseClient(token).get_esi_client(version='latest', response=True)

    seat_char = SeatCharacter.objects.get(character__character_id=character_id)

    notifications, result = c.Character.get_characters_character_id_notifications(character_id=character_id).result()
    last_five_hundred = list(Notification.objects.filter(character=seat_char)[:500].values_list('notification_id', flat=True))

    mass_notifications = []
    for note in notifications:
        if not note.get('notification_id') in last_five_hundred:
            mass_notifications.append(Notification(character=seat_char,
                                                   notification_id=note.get('notification_id'),
                                                   sender_id=note.get('sender_id'),
                                                   sender_type=note.get('sender_type'),
                                                   notification_text=note.get('text'),
                                                   timestamp=note.get('timestamp'),
                                                   notification_type=note.get('type'),
                                                   is_read=note.get('is_read')))

    Notification.objects.bulk_create(mass_notifications, batch_size=500)

    return "Finished notifications for: %s" % (str(character_id))


@shared_task
def update_character_wallet_journal(character_id, full_update=False):  # pagnated results
    logger.debug("updating wallet trans for: %s" % str(character_id))

    req_scopes = ['esi-wallet.read_character_wallet.v1']

    token = _get_token(character_id, req_scopes)
    c = EsiResponseClient(token).get_esi_client(version='latest', response=True)

    seat_char = SeatCharacter.objects.get(character__character_id=character_id)

    bulk_wallet_items = []
    wallet_page = 1
    total_pages = 1
    max_pages = 10  # TODO make this better

    if full_update:
        max_pages = 999

    last_thirty_days = list(CharacterWalletJournalEntry.objects.filter(
        character=seat_char,
        date__gt=(datetime.datetime.utcnow().replace(tzinfo=timezone.utc) - datetime.timedelta(days=30))) \
                            .values_list('entry_id', flat=True))

    while wallet_page <= total_pages and wallet_page < max_pages:
        journal, result = c.Wallet.get_characters_character_id_wallet_journal(character_id=character_id,
                                                                              page=wallet_page).result()
        total_pages = int(result.headers['X-Pages'])

        for _transaction in journal:
            if not _transaction.get('id') in last_thirty_days:
                bulk_wallet_items.append(CharacterWalletJournalEntry(
                    character=seat_char,
                    entry_id=_transaction.get('id'),
                    amount=_transaction.get('amount'),
                    balance=_transaction.get('balance'),
                    context_id=_transaction.get('context_id'),
                    context_id_type=_transaction.get('context_id_type'),
                    date=_transaction.get('date'),
                    description=_transaction.get('description'),
                    first_party_id=_transaction.get('first_party_id'),
                    reason=_transaction.get('reason'),
                    ref_type=_transaction.get('ref_type'),
                    second_party_id=_transaction.get('second_party_id'),
                    tax=_transaction.get('tax'),
                    tax_reciever_id=_transaction.get('tax_reciever_id')))

        wallet_page += 1

        CharacterWalletJournalEntry.objects.bulk_create(bulk_wallet_items, batch_size=500)

    return "Finished wallet trans for: %s" % (str(character_id))


@shared_task
def update_character_market_orders(character_id):
    logger.debug("updating market orders for: %s" % str(character_id))

    req_scopes = ['esi-markets.read_character_orders.v1']

    token = _get_token(character_id, req_scopes)
    c = token.get_esi_client()
    seat_char = SeatCharacter.objects.get(character__character_id=character_id)

    journal = c.Market.get_characters_character_id_orders(character_id=character_id).result()
    order_ids = []
    for order in journal:
        order_ids.append(order.get('order_id'))
        journal_item, created = CharacterMarketOrder.objects \
            .update_or_create(character=seat_char, order_id=order.get('order_id'),
                              defaults={'duration': order.get('duration'),
                                        'escrow': order.get('escrow'),
                                        'is_corporation': order.get('is_corporation'),
                                        'is_buy_order': order.get('is_buy_order'),
                                        'issued': order.get('issued'),
                                        'location_id': order.get('location_id'),
                                        'min_volume': order.get('min_volume'),
                                        'price': order.get('price'),
                                        'range': order.get('range'),
                                        'region_id': order.get('region_id'),
                                        'type_id': order.get('type_id'),
                                        'volume_remain': order.get('volume_remain'),
                                        'volume_total': order.get('volume_total')})

    CharacterMarketOrder.objects.filter(character=seat_char).exclude(order_id__in=order_ids).delete()


@shared_task
def update_character_contacts(character_id):
    logger.debug("updating contacts for: %s" % str(character_id))

    def _update_contact_db(_seat_char, _contact):
        _contact_item, _created = CharacterContact.objects \
            .update_or_create(character=_seat_char, contact_id=_contact.get('contact_id'),
                              defaults={'contact_type': _contact.get('contact_type'),
                                        'standing': _contact.get('standing'),
                                        'blocked': _contact.get('is_blocked', False),
                                        'watched': _contact.get('is_watched', False)})

        if _contact.get('label_ids', False):  # add labels
            for _label in _contact.get('label_ids'):
                _contact_item.labels.add(CharacterContactLabel.objects.get(character=_seat_char, label_id=_label))

            _contact_item.save()

    req_scopes = ['esi-characters.read_contacts.v1']

    token = _get_token(character_id, req_scopes)
    c = EsiResponseClient(token).get_esi_client(version='latest', response=True)
    seat_char = SeatCharacter.objects.get(character__character_id=character_id)

    labels, result = c.Contacts.get_characters_character_id_contacts_labels(character_id=character_id).result()
    label_id_filter = []

    for label in labels:  # update labels
        label_id_filter.append(label.get('order_id'))
        label_item, created = CharacterContactLabel.objects \
            .update_or_create(character=seat_char, label_id=label.get('label_id'),
                              defaults={'label_name': label.get('label_name')})

    contact_page = 1
    total_pages = 1
    contact_ids = []
    while contact_page <= total_pages:  # get all the pages...
        contacts_page, result = c.Contacts.get_characters_character_id_contacts(character_id=character_id,
                                                                                page=contact_page).result()
        total_pages = int(result.headers['X-Pages'])

        for contact in contacts_page:  # update contacts
            contact_ids.append(contact.get('contact_id'))
            _update_contact_db(seat_char, contact)

        contact_page += 1

    CharacterContact.objects.filter(character=seat_char).exclude(
        contact_id__in=contact_ids).delete()  # delete old stuff
    CharacterContactLabel.objects.filter(character=seat_char).exclude(label_id__in=label_id_filter).delete()


@shared_task
def update_character_fittings(character_id):
    logger.debug("updating fits for: %s" % str(character_id))

    def _fitting_db_update(_seat_char, _fitting):
        _fit_db_item, _created = Fitting.objects.update_or_create(
            character=_seat_char,
            fitting_id=_fitting.get('fitting_id'),
            defaults={'name': _fitting.get('name'),
                      'ship_type_id': _fitting.get('ship_type_id'),
                      'description': _fitting.get('description')})

        if not _created:
            FittingItem.objects.filter(fit=_fit_db_item).delete()

        for _fit_item in _fitting.get('items', []):
            FittingItem.objects.create(fit=_fit_db_item,
                                       flag=_fit_item.get('flag'),
                                       quantity=_fit_item.get('quantity'),
                                       type_id=_fit_item.get('type_id'))

        return _fit_db_item, _created

    req_scopes = ['esi-fittings.read_fittings.v1']

    token = _get_token(character_id, req_scopes)
    c = token.get_esi_client()

    seat_char = SeatCharacter.objects.get(character__character_id=character_id)

    fit_ids = []
    fittings = c.Fittings.get_characters_character_id_fittings(character_id=character_id).result()

    for fitting in fittings:
        fit_ids.append(fitting.get('fitting_id'))
        _fitting_db_update(seat_char, fitting)  # return'd values not needed

    Fitting.objects.filter(character=seat_char).exclude(fitting_id__in=fit_ids).delete()


@shared_task
def update_character_mining(character_id):
    logger.debug("updating mining ledger for: %s" % str(character_id))

    def _mining_db_update(_seat_char, _mined):
        _mining_item, _created = CharacterMiningLedger.objects \
            .update_or_create(character=_seat_char, type_id=_mined.get('type_id'), date=_mined.get('date'),
                              defaults={'quantity': _mined.get('quantity'),
                                        'solar_system_id': _mined.get('solar_system_id')})
        return _mining_item, _created

    req_scopes = ['esi-industry.read_character_mining.v1']

    token = _get_token(character_id, req_scopes)
    c = EsiResponseClient(token).get_esi_client(version='latest', response=True)

    seat_char = SeatCharacter.objects.get(character__character_id=character_id)

    mining_page = 1
    total_pages = 1
    while mining_page <= total_pages:
        mining_list, result = c.Industry.get_characters_character_id_mining(character_id=character_id,
                                                                            page=mining_page).result()
        total_pages = int(result.headers['X-Pages'])

        for mined in mining_list:
            _mining_db_update(seat_char, mined)  # return'd values not needed

        mining_page += 1


@shared_task
def update_character_standings(character_id):
    logger.debug("updating standings for: %s" % str(character_id))

    def _standing_db_update(_seat_char, _standing):
        _standing_item, _created = Standing.objects \
            .update_or_create(character=_seat_char, from_id=_standing.get('from_id'),
                              defaults={'from_type': _standing.get('from_type'),
                                        'standing': _standing.get('standing')})
        return _standing_item, _created

    req_scopes = ['esi-characters.read_standings.v1']

    token = _get_token(character_id, req_scopes)
    c = token.get_esi_client()

    seat_char = SeatCharacter.objects.get(character__character_id=character_id)

    standing_list = c.Character.get_characters_character_id_standings(character_id=character_id).result()

    for standing in standing_list:
        _standing_db_update(seat_char, standing)  # return'd values not needed


@shared_task
def update_names_from_sde():
    ItemName.objects.all().delete()
    TypeName.objects.all().delete()
    # Get needed SDE files
    invNames_url = 'https://www.fuzzwork.co.uk/dump/latest/invNames.csv.bz2'
    invTypes_url = 'https://www.fuzzwork.co.uk/dump/latest/invTypes.csv.bz2'

    invNames_req = requests.get(invNames_url)
    with open('invNames.csv.bz2', 'wb') as iN:
        iN.write(invNames_req.content)
    invTypes_req = requests.get(invTypes_url)
    with open('invTypes.csv.bz2', 'wb') as iT:
        iT.write(invTypes_req.content)

    # Decompress SDE files
    open('invNames.csv', 'wb').write(bz2.open('invNames.csv.bz2', 'rb').read())
    open('invTypes.csv', 'wb').write(bz2.open('invTypes.csv.bz2', 'rb').read())

    # Parse file(s) and Update names object(s)
    item_names = []
    with open('invNames.csv', 'r', encoding='UTF-8') as iN:
        csv_list = iN.read().split('\n')
        for row in csv_list[1:]:
            spl = row.split(',')
            if len(spl) > 1:
                item_names.append(ItemName(item_id=int(spl[0]), name=spl[1]))

    ItemName.objects.bulk_create(item_names, batch_size=500)

    type_names = []
    with open('invTypes.csv', 'r', encoding='UTF-8') as iT:
        # Remove Descriptions
        text = re.sub(r'\"((.|\n)*?)\"', 'None', iT.read())
        # Process file
        csv_list = text.split('\n')
        for row in csv_list[2:]:
            spl = row.split(',')
            if len(spl) > 1:
                type_names.append(TypeName(type_id=int(spl[0]), name=spl[2]))

    TypeName.objects.bulk_create(type_names, batch_size=500)


@shared_task
def _get_insert_eve_names(chunk: list):
    logger.debug("Getting and creating EveNames for ids: %s to %s" % (str(chunk[0]), str(chunk[-1])))

    c = esi_client_factory(version='v3')
    names = c.Universe.post_universe_names(ids=chunk).result()

    objs = []
    for name in names:
        objs.append(EveName(name=name.get('name'), name_id=name.get('id'), category=name.get('category')))

    EveName.objects.bulk_create(objs)


@shared_task
def update_eve_names():
    logger.debug("Getting IDs for EVE Name update")
    # Gather IDs
    history_corps = set([corp.corporation_id for corp in CorpHistory.objects.all().only('corporation_id')])
    standing_ids = set([from_.from_id for from_ in Standing.objects.all().only('from_id').exclude(from_type='agent')])
    contact_ids = set([con.contact_id for con in CharacterContact.objects.all().only('contact_id')] +
                      [con.contact_id for con in CorporationContact.objects.all().only('contact_id')])
    recipient_ids = set([r.recipient_id for r in MailRecipient.objects.all().only('recipient_id')
                        .exclude(recipient_type='mailing_list')])
    not_senders = set([s.sender_id for s in Notification.objects.all().only('sender_id')])
    journal_ids = set([j.first_party_id for j in CharacterWalletJournalEntry.objects.all().only('first_party_id')]
                      + [j.second_party_id for j in CharacterWalletJournalEntry.objects.all().only('second_party_id')])
    corp_j_ids = set([j.first_party_id for j in CorporationWalletJournalEntry.objects.all().only('first_party_id')]
                      + [j.second_party_id for j in CorporationWalletJournalEntry.objects.all().only('second_party_id')])
    client_ids = set([c.client_id for c in CharacterWalletTransactionEntry.objects.all().only('client_id')]
                     + [c.client_id for c in CorporationWalletTransactionEntry.objects.all().only('client_id')])
    ccontract_ids = set([a.acceptor_id for a in CharacterContract.objects.all().only('acceptor_id')] +
                        [a.asignee_id for a in CharacterContract.objects.all().only('assignee_id')] +
                        [i.issuer_id for i in CharacterContract.objects.all().only('issuer_id')])
    contract_ids = set([a.acceptor_id for a in CorporationContract.objects.all().only('acceptor_id')] +
                       [a.asignee_id for a in CorporationContract.objects.all().only('assignee_id')] +
                       [i.issuer_id for i in CorporationContract.objects.all().only('issuer_id')])

    # Join all the sets
    set_ids = set().union(history_corps, standing_ids, contact_ids, not_senders, journal_ids,
                          corp_j_ids, client_ids, ccontract_ids, contract_ids, recipient_ids)
    set_copy = set_ids.copy()

    # Remove IDs that are already in eve names
    names = [item.name_id for item in EveName.objects.all().only('name_id')]
    for i in set_copy:
        if i in names:
            set_ids.discard(i)

    ids = list(set_ids)

    # Chunk IDs
    chunks = [ids[i:i + 500] for i in range(0, len(ids), 500)]
    for chunk in chunks:
        _get_insert_eve_names.delay(chunk)

    return


@shared_task
def update_character_assets(character_id):
    logger.debug("updating assets for: %s" % str(character_id))

    def _asset_db_update(_seat_char, _asset):
        _asset, _created = CharacterAsset.objects. \
            update_or_create(character=_seat_char,
                             item_id=_asset.get('item_id'),
                             defaults={'blueprint_copy': _asset.get('is_blueprint_copy', None),
                                       'singleton': _asset.get('is_singleton'),
                                       'item_id': _asset.get('item_id'),
                                       'location_flag': _asset.get('location_flag'),
                                       'location_id': _asset.get('location_id'),
                                       'location_type': _asset.get('location_type'),
                                       'quantity': _asset.get('quantity'),
                                       'type_id': _asset.get('type_id'),
                                       'name': _asset.get('name', None)})

        return _asset, _created

    req_scopes = ['esi-assets.read_assets.v1']
    token = _get_token(character_id, req_scopes)
    c = EsiResponseClient(token).get_esi_client(version='latest', response=True)

    seat_char = SeatCharacter.objects.get(character__character_id=character_id)

    asset_page = 1
    total_pages = 1
    asset_ids = []
    while asset_page <= total_pages:
        asset_list, result = c.Assets.get_characters_character_id_assets(character_id=character_id,
                                                                         page=asset_page).result()
        total_pages = int(result.headers['X-Pages'])

        for asset in asset_list:
            _asset_db_update(seat_char, asset)  # return'd values not needed
            asset_ids.append(asset.get('item_id'))
        asset_page += 1

    CharacterAsset.objects.filter(character=seat_char).exclude(item_id__in=asset_ids).delete()


@shared_task
def update_character_calendar_events(character_id):
    logger.debug('updating calendar events for: %s' % str(character_id))

    def _calendar_event_db_update(_seat_char, _event):
        _calendar_event, _created = CalendarEvent.objects.update_or_create(character=_seat_char,
                                                                           event_id=_event.get('event_id'),
                                                                           defaults={
                                                                               'date': event.get('date'),
                                                                               'duration': event.get('duration'),
                                                                               'importance': event.get('importance'),
                                                                               'owner_id': event.get('owner_id'),
                                                                               'owner_name': event.get('owner_name'),
                                                                               'owner_type': event.get('owner_type'),
                                                                               'response': event.get('response'),
                                                                               'text': event.get('text'),
                                                                               'title': event.get('title')
                                                                           })
        return _calendar_event, _created

    req_scopes = ['esi-calendar.read_calendar_events.v1']

    seat_char = SeatCharacter.objects.get(character__character_id=character_id)

    token = _get_token(character_id, req_scopes)
    c = token.get_esi_client()

    ids = c.Calendar.get_characters_character_id_calendar(character_id=character_id).result()
    id_list = [id_.get('event_id') for id_ in ids]
    for eid in id_list:
        event = c.Calendar.get_characters_character_id_calendar_event_id(character_id=character_id,
                                                                         event_id=eid).result()
        _calendar_event_db_update(seat_char, event)


@shared_task
def update_character_clones(character_id):
    logger.debug("updating clones for: %s" % str(character_id))

    req_scopes = ['esi-clones.read_clones.v1', 'esi-clones.read_implants.v1']

    seat_char = SeatCharacter.objects.get(character__character_id=character_id)

    token = _get_token(character_id, req_scopes)
    c = token.get_esi_client()

    # Delete all clones for the character
    JumpClone.objects.filter(character=seat_char).delete()

    clones = c.Clones.get_characters_character_id_clones(character_id=character_id).result()
    jump_clones = clones.get('jump_clones')
    for clone in jump_clones:
        if clone.get('name') is 0:
            clone_name = None
        else:
            clone_name = clone.get('name')
        jump_clone, _created = JumpClone.objects.get_or_create(character=seat_char,
                                                               jump_clone_id=clone.get('jump_clone_id'),
                                                               location_id=clone.get('location_id'),
                                                               location_type=clone.get('location_type'),
                                                               name=clone_name)

        for implant in clone.get('implants'):
            obj, _created = Implant.objects.get_or_create(implant_id=implant)
            jump_clone.implants.add(obj)
            jump_clone.save()

    # Clear Implant Relations for SeatCharacter
    seat_char.implants.clear()

    implants = c.Clones.get_characters_character_id_implants(character_id=character_id).result()
    for implant in implants:
        obj, _created = Implant.objects.get_or_create(implant_id=implant)
        seat_char.implants.add(obj)
        seat_char.save()


@shared_task
def update_character_industry_jobs(character_id):
    logger.debug("updating clones for: %s" % str(character_id))

    req_scopes = ['esi-industry.read_character_jobs.v1']

    seat_char = SeatCharacter.objects.get(character__character_id=character_id)

    token = _get_token(character_id, req_scopes)
    c = token.get_esi_client()

    jobs = c.Industry.get_characters_character_id_industry_jobs(character_id=character_id).result()

    def _jobs_db_update(_seat_char, _job):
        _indy_job, _created = CharacterIndustryJob.objects\
            .update_or_create(character=_seat_char, job_id=_job.get('job_id'),
                              defaults={
                                  'activity_id': _job.get('activity_id'),
                                  'status': _job.get('status'),
                                  'probability': _job.get('probability', None),
                                  'runs': _job.get('runs'),
                                  'successful_runs': _job.get('successful_runs', None),
                                  'cost': _job.get('cost', None),
                                  'duration': _job.get('duration'),
                                  'blueprint_id': _job.get('blueprint_id'),
                                  'blueprint_type_id': _job.get('blueprint_type_id'),
                                  'product_type_id': _job.get('product_type_id', None),
                                  'licensed_runs': _job.get('licensed_runs', None),
                                  'start_date': _job.get('start_date'),
                                  'pause_date': _job.get('pause_date', None),
                                  'end_date': _job.get('end_date'),
                                  'completed_date': _job.get('completed_date', None),
                                  'installer_id': _job.get('installer_id'),
                                  'completed_character_id': _job.get('completed_character_id', None),
                                  'blueprint_location_id': _job.get('blueprint_location_id'),
                                  'facility_id': _job.get('facility_id'),
                                  'output_location_id': _job.get('output_location_id'),
                                  'station_id': _job.get('station_id')
                              })
    for job in jobs:
        _jobs_db_update(seat_char, job)


@shared_task
def process_mail_list(character_id: int, ids: list):
    """
    Takes list of mail ids to process
    :param character_id: int
    :param ids: list of ids to process
    :return: Nothing
    """
    req_scopes = ['esi-mail.read_mail.v1']

    seat_char = SeatCharacter.objects.get(character__character_id=character_id)

    token = _get_token(character_id, req_scopes)
    c = token.get_esi_client()

    messages = []
    m_l_map = {}
    m_r_map = {}
    for id in ids:
        msg = c.Mail.get_characters_character_id_mail_mail_id(character_id=character_id, mail_id=id).result()
        id_k = int(str(seat_char.character.character_id)+str(id))
        msg_obj = MailMessage(character=seat_char, id_key=id_k, mail_id=id, from_id=msg.get('from', None),
                              is_read=msg.get('read', None), timestamp=msg.get('timestamp'),
                              subject=msg.get('subject', None), body=msg.get('body', None))
        messages.append(msg_obj)
        if msg.get('labels'):
            labels = msg.get('labels')
            m_l_map[str(id)] = labels
        m_r_map[str(id)] = [r.get('recipient_id') for r in msg.get('recipients')]
    logger.debug("Message Objects for {} to {} created".format(ids[0], ids[-1]))
    msgs = MailMessage.objects.bulk_create(messages)
    LabelThroughModel = MailMessage.labels.through
    lms = []
    for msg in msgs:
        if str(msg.mail_id) in m_l_map:
            for label in m_l_map[str(msg.mail_id)]:
                lm = LabelThroughModel(mailmessage_id=msg.id_key,
                                       maillabel_id=MailLabel.objects.get(character=seat_char, label_id=label).pk)
                lms.append(lm)
    LabelThroughModel.objects.bulk_create(lms)
    RecipThroughModel = MailMessage.recipients.through
    rms = []
    for msg in msgs:
        if str(msg.mail_id) in m_r_map:
            for recip in m_r_map[str(msg.mail_id)]:
                rm = RecipThroughModel(mailmessage_id=msg.id_key, mailrecipient_id=recip)
                rms.append(rm)
    RecipThroughModel.objects.bulk_create(rms)

    return "Completed mail fetch for: %s" % str(character_id)


@shared_task
def update_character_mail(character_id):
    #  This function will deal with ALL mail related updates
    logger.debug("updating mail for: %s" % str(character_id))

    req_scopes = ['esi-mail.read_mail.v1']

    seat_char = SeatCharacter.objects.get(character__character_id=character_id)

    token = _get_token(character_id, req_scopes)
    c = token.get_esi_client()

    # Mail Labels
    labels = c.Mail.get_characters_character_id_mail_labels(character_id=character_id).result()

    def _update_mail_label_db(_seat_char, _label):
        _mail_label, _created = MailLabel.objects.update_or_create(character=_seat_char,
                                                                   label_id=_label.get('label_id'),
                                                                   defaults={
                                                                       'label_name': _label.get('name', None),
                                                                       'label_color': _label.get('color', None),
                                                                       'unread_count': _label.get('unread_coun, Nonet')
                                                                   })

    for label in labels.get('labels'):
        _update_mail_label_db(seat_char, label)

    # Get all mail ids and make sure that recipients exist
    mail_ids = []
    try:
        last_id_db = MailMessage.objects.filter(character=seat_char).order_by('-mail_id')[0].mail_id
    except IndexError:
        last_id_db = None

    last_id = None
    fp = True
    while True:
        if last_id is None:
            mail = c.Mail.get_characters_character_id_mail(character_id=character_id).result()
        else:
            fp = False
            mail = c.Mail.get_characters_character_id_mail(character_id=character_id, last_mail_id=last_id).result()
        if len(mail) == 0 and fp is False:
            # If there are 0 and this is not the first page, then we have reached the
            # end of retrievable mail.
            break

        stop = False
        for msg in mail:
            if msg.get('mail_id') == last_id_db:
                # Break both loops if we have reached the most recent mail in the db.
                print("Found {} in database for user {}.".format(msg.get('mail_id'), character_id))
                stop = True
                break

            mail_ids.append(msg.get('mail_id'))
            for recip in msg.get('recipients'):
                MailRecipient.objects.get_or_create(recipient_id=recip.get('recipient_id'),
                                                    recipient_type=recip.get('recipient_type'))
            last_id = msg.get('mail_id')
        if stop is True:
            # Break the while loop if we reach the last mail message that is in the db.
            break

    if len(mail_ids) is 0:
        logger.debug("No new mails for {}".format(character_id))
        return

    # Get and Create messages
    chunks = [mail_ids[i:i + 500] for i in range(0, len(mail_ids), 500)]
    for chunk in chunks:
        # Process mails in chunks of 500
        process_mail_list.delay(character_id, chunk)

    return "Completed mail pre-fetch for: %s" % str(character_id)


@shared_task
def process_character_contract_items(character_id: int, ids: list):
    """
    Takes list of contract ids to fetch items
    :param character_id: int
    :param ids: list of contract ids to process
    :return: Nothing of importance
    """
    logger.debug("fetching contract items for: %s" % str(character_id))

    req_scopes = ['esi-contracts.read_character_contracts.v1']

    seat_char = SeatCharacter.objects.get(character__character_id=character_id)

    token = _get_token(character_id, req_scopes)
    c = token.get_esi_client()

    bulk_contract_item_list = []
    for _contract in ids:
        contract_item=CharacterContract.objects.get(
                                                    character=seat_char,
                                                    contract_id=_contract
                                                    )

        _items = c.Contracts.get_characters_character_id_contracts_contract_id_items(
            character_id=character_id,contract_id=_contract).result()

        for _item in _items:
            bulk_contract_item_list.append(
                CharacterContractItem(
                    contract=contract_item,
                    included = _item.get('is_included'),
                    singleton = _item.get('is_singleton'),
                    quantity = _item.get('quantity'),
                    raw_quantity = _item.get('raw_quantity'),
                    record_id = _item.get('record_id'),
                    type_id = _item.get('type_id')
                )
            )

    CharacterContractItem.objects.bulk_create(bulk_contract_item_list, batch_size=500)

    return "Completed contract items fetch for: %s" % str(character_id)


@shared_task
def update_character_contracts(character_id):
    #  This function will deal with ALL mail related updates
    logger.debug("updating contracts for: %s" % str(character_id))

    req_scopes = ['esi-contracts.read_character_contracts.v1']

    seat_char = SeatCharacter.objects.get(character__character_id=character_id)

    token = _get_token(character_id, req_scopes)
    c = EsiResponseClient(token).get_esi_client(version='latest', response=True)

    inactive_contracts = list(CharacterContract.objects.filter(
        Q(status__icontains='finished_issuer') | Q(status__icontains='finished_contractor') |
        Q(status__icontains='finished') | Q(status__icontains='cancelled') | Q(status__icontains='failed') |
        Q(status__icontains='deleted') | Q(status__icontains='reversed'),
        character=seat_char
        ).values_list('contract_id', flat=True))
    all_contracts = list(CharacterContract.objects.filter(
        character=seat_char
        ).values_list('contract_id', flat=True))

    bulk_contract_ids = []
    bulk_contract_items = []
    contract_page = 1
    total_pages = 1
    while contract_page <= total_pages:
        contracts, result = c.Contracts.get_characters_character_id_contracts(character_id=character_id,
                                                                              page=contract_page).result()
        total_pages = int(result.headers['X-Pages'])
        for _contract in contracts:
            if not _contract.get('contract_id') in all_contracts:
                bulk_contract_items.append(CharacterContract(
                    character=seat_char,
                    acceptor_id=_contract.get('acceptor_id'),
                    assignee_id=_contract.get('assignee_id'),
                    availability=_contract.get('availability'),
                    buyout=_contract.get('buyout'),
                    collateral=_contract.get('collateral'),
                    contract_id=_contract.get('contract_id'),
                    date_accepted=_contract.get('date_accepted'),
                    date_completed=_contract.get('date_completed'),
                    date_expired=_contract.get('date_expired'),
                    days_to_complete=_contract.get('days_to_complete'),
                    end_location_id=_contract.get('end_location_id'),
                    for_corporation=_contract.get('for_corporation'),
                    issuer_corporation_id=_contract.get('issuer_corporation_id'),
                    issuer_id=_contract.get('issuer_id'),
                    price=_contract.get('price'),
                    reward=_contract.get('reward'),
                    start_location_id=_contract.get('start_location_id'),
                    status=_contract.get('status'),
                    title=_contract.get('title'),
                    contract_type=_contract.get('type'),
                    volume=_contract.get('volume')))
                bulk_contract_ids.append(_contract.get('contract_id'))
            else:
                if _contract.get('contract_id')not in inactive_contracts:
                    if __name__ == '__main__':
                        CharacterContract.objects.filter(character=seat_char,
                                                         contract_id=_contract.get('contract_id')).update(
                            acceptor_id=_contract.get('acceptor_id'),
                            assignee_id=_contract.get('assignee_id'),
                            date_accepted=_contract.get('date_accepted'),
                            date_completed=_contract.get('date_completed'),
                            date_expired=_contract.get('date_expired'),
                            status=_contract.get('status'),
                        )

        contract_page += 1

    CharacterContract.objects.bulk_create(bulk_contract_items, batch_size=500)

    # Get and Create messages
    chunks = [bulk_contract_ids[i:i + 500] for i in range(0, len(bulk_contract_ids), 500)]  # realistically i doubt it
    for chunk in chunks:
        # Process chunks of 500
        process_character_contract_items.delay(character_id, chunk)

    return "Completed contract pre-fetch for: %s" % str(character_id)


@shared_task
def update_corp_assets(character_id):
    logger.debug("Started assets for: %s" % str(character_id))

    def _asset_create(_corp, _asset):
        return CorpAsset(corp=_corp,
                         item_id=_asset.get('item_id'),
                         blueprint_copy=_asset.get('is_blueprint_copy', None),
                         singleton=_asset.get('is_singleton'),
                         location_flag=_asset.get('location_flag'),
                         location_id=_asset.get('location_id'),
                         location_type=_asset.get('location_type'),
                         quantity=_asset.get('quantity'),
                         type_id=_asset.get('type_id'),
                         name=_asset.get('name', None))

    req_scopes = ['esi-assets.read_corporation_assets.v1', 'esi-characters.read_corporation_roles.v1']
    req_roles = ['CEO', 'Director']

    token = _get_token(character_id, req_scopes)

    c = EsiResponseClient(token).get_esi_client(response=True)
    # check roles!
    roles, result = c.Character.get_characters_character_id_roles(character_id=character_id).result()

    has_roles = False
    for role in roles.get('roles', []):
        if role in req_roles:
            has_roles = True

    if not has_roles:
        return "No Roles on Character"

    _character = SeatCharacter.objects.get(character__character_id=character_id)
    _corporation = _character.character.corporation

    CorpAsset.objects.filter(corp=_corporation).delete()  # Nuke!

    assets_for_insert = []
    asset_page = 1
    total_pages = 1
    while asset_page <= total_pages:
        asset_list, result = c.Assets.get_corporations_corporation_id_assets(corporation_id=_corporation.corporation_id,
                                                                             page=asset_page).result()
        total_pages = int(result.headers['X-Pages'])

        for asset in asset_list:
            assets_for_insert.append(_asset_create(_corporation, asset))
        asset_page += 1

    CorpAsset.objects.bulk_create(assets_for_insert, batch_size=500)

    return "Finished assets for: %s" % (str(character_id))


@shared_task
def update_corp_wallet_journal(character_id, wallet_division, full_update=False):  # pagnated results
    logger.debug("Started wallet trans for: %s (%s)" % (str(character_id), str(wallet_division)))

    def journal_db_update(_transaction, _division, existing_id):

        if not _transaction.get('id') in existing_id:
            _journal_item = CorporationWalletJournalEntry(
                division=_division,
                entry_id=_transaction.get('id'),
                amount=_transaction.get('amount'),
                balance=_transaction.get('balance'),
                context_id=_transaction.get('context_id'),
                context_id_type=_transaction.get('context_id_type'),
                date=_transaction.get('date'),
                description=_transaction.get('description'),
                first_party_id=_transaction.get('first_party_id'),
                reason=_transaction.get('reason'),
                ref_type=_transaction.get('ref_type'),
                second_party_id=_transaction.get('second_party_id'),
                tax=_transaction.get('tax'),
                tax_reciever_id=_transaction.get('tax_reciever_id'))

            return _journal_item
        else:
            return False

    req_scopes = ['esi-wallet.read_corporation_wallets.v1', 'esi-characters.read_corporation_roles.v1']
    req_roles = ['CEO', 'Director', 'Accountant', 'Junior_Accountant']

    token = _get_token(character_id, req_scopes)
    c = EsiResponseClient(token).get_esi_client(response=True)

    # check roles!
    roles, result = c.Character.get_characters_character_id_roles(character_id=character_id).result()

    has_roles = False
    for role in roles.get('roles', []):
        if role in req_roles:
            has_roles = True

    if not has_roles:
        return "No Roles on Character"

    _character = SeatCharacter.objects.get(character__character_id=character_id)
    _corporation = _character.character.corporation
    _division = CorporationWalletDivision.objects.get(corporation=_corporation, division=wallet_division)

    bulk_wallet_items = []
    wallet_page = 1
    total_pages = 1
    last_thrity = list(CorporationWalletJournalEntry.objects.filter(
        date__gt=(datetime.datetime.utcnow().replace(tzinfo=timezone.utc) - datetime.timedelta(days=60))).values_list(
        'entry_id', flat=True))
    while wallet_page <= total_pages:
        journal, result = c.Wallet.get_corporations_corporation_id_wallets_division_journal(
            corporation_id=_corporation.corporation_id,
            division=wallet_division,
            page=wallet_page).result()
        total_pages = int(result.headers['X-Pages'])

        for transaction in journal:
            _j_ob = journal_db_update(transaction, _division, last_thrity)
            if _j_ob:
                bulk_wallet_items.append(_j_ob)  # return'd values not needed
            else:
                # old!
                if not full_update:
                    wallet_page = total_pages
                    break

        wallet_page += 1

    # TODO pass name_ids to a new task to do names
    CorporationWalletJournalEntry.objects.bulk_create(bulk_wallet_items, batch_size=500)

    return "Finished wallet trans for: %s" % (str(character_id))


@shared_task
def update_corp_wallet_division(character_id, full_update=False):  # pagnated results
    logger.debug("Started wallet divs for: %s" % str(character_id))

    req_scopes = ['esi-wallet.read_corporation_wallets.v1', 'esi-characters.read_corporation_roles.v1']
    req_roles = ['CEO', 'Director', 'Accountant', 'Junior_Accountant']

    token = _get_token(character_id, req_scopes)
    c = token.get_esi_client()

    # check roles!
    roles = c.Character.get_characters_character_id_roles(character_id=character_id).result()

    has_roles = False
    for role in roles.get('roles', []):
        if role in req_roles:
            has_roles = True

    if not has_roles:
        return "No Roles on Character"

    _character = SeatCharacter.objects.get(character__character_id=character_id)
    _corporation = _character.character.corporation

    _divisions = c.Wallet.get_corporations_corporation_id_wallets(corporation_id=_corporation.corporation_id).result()

    for division in _divisions:
        _division_item, _created = CorporationWalletDivision.objects \
            .update_or_create(corporation=_corporation, division=division.get('division'),
                              defaults={'balance': division.get('balance')})

        if _division_item:
            update_corp_wallet_journal(character_id, division.get('division'),
                                       full_update=full_update)  # inline not async

    return "Finished wallet divs for: %s" % (str(character_id))


@shared_task
def update_corp_structures(character_id):  # pagnated results
    logger.debug("Started structures for: %s" % (str(character_id)))

    def _structures_db_update(_corporation, _structure, _name):
        _structure_ob, _created = Structure.objects.update_or_create(
            structure_id=_structure.get('structure_id'),
            corporation=_corporation,
            defaults={
                'fuel_expires': _structure.get('fuel_expires', None),
                'next_reinforce_apply': _structure.get('next_reinforce_apply', None),
                'next_reinforce_weekday': _structure.get('next_reinforce_weekday', None),
                'profile_id': _structure.get('profile_id', None),
                'reinforce_hour': _structure.get('reinforce_hour', None),
                'reinforce_weekday': _structure.get('reinforce_weekday', None),
                'state': _structure.get('state', None),
                'state_timer_end': _structure.get('state_timer_end', None),
                'state_timer_start': _structure.get('state_timer_start', None),
                'system_id': _structure.get('system_id', None),
                'type_id': _structure.get('type_id', None),
                'unanchors_at': _structure.get('unanchors_at', None),
                'name': _name
            })

        if _structure_ob:
            if _structure.get('services'):
                StructureService.objects.filter(structure=_structure_ob).delete()
                for service in _structure.get('services'):
                    _service_ob = StructureService.objects.create(
                        structure=_structure_ob,
                        state=service['state'],
                        name=service['name'])

        return _structure_ob, _created

    req_scopes = ['esi-corporations.read_structures.v1', 'esi-universe.read_structures.v1',
                  'esi-characters.read_corporation_roles.v1']

    req_roles = ['CEO', 'Director', 'Station_Manager']

    token = _get_token(character_id, req_scopes)
    c = EsiResponseClient(token).get_esi_client(response=True)

    # check roles!
    roles, result = c.Character.get_characters_character_id_roles(character_id=character_id).result()

    has_roles = False
    for role in roles.get('roles', []):
        if role in req_roles:
            has_roles = True

    if not has_roles:
        return "No Roles on Character"

    _character = SeatCharacter.objects.get(character__character_id=character_id)
    _corporation = _character.character.corporation
    structure_ids = []
    structure_pages = 1
    total_pages = 1
    while structure_pages <= total_pages:
        structures, result = c.Corporation.get_corporations_corporation_id_structures(
            corporation_id=_corporation.corporation_id,
            page=structure_pages).result()

        total_pages = int(result.headers['X-Pages'])

        for structure in structures:
            try:
                structure_info, result = c.Universe.get_universe_structures_structure_id(
                    structure_id=structure.get('structure_id')).result()
            except:  # if bad screw it...
                structure_info = False
            structure_ob, created = _structures_db_update(_corporation,
                                                          structure,
                                                          structure_info['name'] if structure_info else str(
                                                              structure.get('structure_id')))
            structure_ids.append(structure_ob.structure_id)

        structure_pages += 1

    Structure.objects.filter(corporation=_corporation).exclude(
        structure_id__in=structure_ids).delete()  # structures die/leave

    return "Finished structures for: %s" % (str(character_id))


@shared_task
def update_character_wallet_transactions(character_id):
    logger.debug('Updating wallet transactions for character %s' % str(character_id))

    def _transaction_db_update(_seat_char, _transaction):
        _entry, _created = CharacterWalletTransactionEntry.objects\
            .update_or_create(character=seat_char, transaction_id=_transaction.get('transaction_id'),
                              defaults={
                                  'client_id': _transaction.get('client_id'),
                                  'date': _transaction.get('date'),
                                  'is_buy': _transaction.get('is_buy'),
                                  'is_personal': _transaction.get('is_personal'),
                                  'journal_ref_id': _transaction.get('journal_ref_id'),
                                  'location_id': _transaction.get('location_id'),
                                  'quantity': _transaction.get('quantity'),
                                  'type_id': _transaction.get('type_id'),
                                  'unit_price': _transaction.get('unit_price')
                              })
        return _entry

    req_scopes = ['esi-wallet.read_character_wallet.v1']
    seat_char = SeatCharacter.objects.get(character__character_id=character_id)

    token = _get_token(character_id, req_scopes)
    c = token.get_esi_client()

    transactions = c.Wallet.get_characters_character_id_wallet_transactions(character_id=character_id).result()

    for transaction in transactions:
        _transaction_db_update(seat_char, transaction)

    return "Finished wallet transactions for: %s" % str(character_id)


@shared_task
def update_corporation_wallet_transactions(character_id):
    logger.debug('Updating corp wallet transactions for: %s' % str(character_id))

    def _transaction_db_update(_seat_char, _transaction, _division):
        _entry, _created = CorporationWalletTransactionEntry.objects\
            .update_or_create(character=seat_char, transaction_id=_transaction.get('transaction_id'), division=division,
                              defaults={
                                  'client_id': _transaction.get('client_id'),
                                  'date': _transaction.get('date'),
                                  'is_buy': _transaction.get('is_buy'),
                                  'journal_ref_id': _transaction.get('journal_ref_id'),
                                  'location_id': _transaction.get('location_id'),
                                  'quantity': _transaction.get('quantity'),
                                  'type_id': _transaction.get('type_id'),
                                  'unit_price': _transaction.get('unit_price')
                              })
        return _entry

    req_scopes = ['esi-wallet.read_corporation_wallets.v1', 'esi-characters.read_corporation_roles.v1']
    req_roles = ['CEO', 'Director', 'Accountant', 'Junior_Accountant']

    seat_char = SeatCharacter.objects.get(character__character_id=character_id)

    token = _get_token(character_id, req_scopes)
    c = token.get_esi_client()

    # check roles!
    roles = c.Character.get_characters_character_id_roles(character_id=character_id).result()

    has_roles = False
    for role in roles.get('roles', []):
        if role in req_roles:
            has_roles = True

    if not has_roles:
        return "No Roles on Character"

    corp = seat_char.character.corporation
    divisions = CorporationWalletDivision.objects.filter(corporation=corp)

    for division in divisions:
        items = c.Wallet.\
            get_corporationss_corporation_id_wallets_division_transactions(corporation_id=corp.corporation_id,
                                                                           division=division).result()

        for transaction in items:
            _transaction_db_update(seat_char, transaction, division)

    return "Finished wallet transactions for: %s" % str(character_id)


@shared_task
def update_corp_mining_observers(character_id):
    logger.debug("Started mining observers for: %s" % str(character_id))

    def _observer_create(_corp, _observer, _last_updated):
        return MiningObserver(corporation=_corp,
                              last_updated=_last_updated,
                              observer_id=_observer.get('observer_id'),
                              observer_type=_observer.get('observer_type'))

    def _observation_create(_observer, _last_updated, _observer_id):
        return MiningObservation(observer=MiningObserver.objects.get(observer_id=_observer_id),
                              character_id=_observer.get('character_id'),
                              last_updated=_last_updated,
                              recorded_corporation_id=_observer.get('recorded_corporation_id'),
                              type_id=_observer.get('type_id'),
                              quantity=_observer.get('quantity'))

    req_scopes = ['esi-industry.read_corporation_mining.v1', 'esi-characters.read_corporation_roles.v1']
    req_roles = ['CEO', 'Director', 'Accountant']

    token = _get_token(character_id, req_scopes)

    c = EsiResponseClient(token).get_esi_client(response=True)
    # check roles!
    roles, result = c.Character.get_characters_character_id_roles(character_id=character_id).result()

    has_roles = False
    for role in roles.get('roles', []):
        if role in req_roles:
            has_roles = True

    if not has_roles:
        return "No Roles on Character"

    _character = SeatCharacter.objects.get(character__character_id=character_id)
    _corporation = _character.character.corporation

    observer_db_list = list(MiningObserver.objects.filter(corporation=_corporation).values_list(
        'observer_id', flat=True))

    new_observers = []
    ob_page = 1
    total_ob_pages = 1
    while ob_page <= total_ob_pages:
        observers, result = c.Industry.get_corporation_corporation_id_mining_observers(
            corporation_id=_corporation.corporation_id,
            page=ob_page).result()

        total_ob_pages = int(result.headers['X-Pages'])

        for observer in observers:
            last_updated_datetime = datetime.datetime(
                observer.get('last_updated').year,
                observer.get('last_updated').month,
                observer.get('last_updated').day).replace(tzinfo=timezone.utc)

            if observer.get('observer_id') not in observer_db_list:

                new_observers.append(_observer_create(_corporation, observer, last_updated_datetime))
                observer_db_list.append(observer.get('observer_id'))
            else:
                MiningObserver.objects.filter(observer_id=observer.get('observer_id')).update(
                    last_updated=last_updated_datetime,
                )
        ob_page += 1

    MiningObserver.objects.bulk_create(new_observers, batch_size=500)


    try:
        latest_db_date = MiningObservation.objects.all().order_by('-last_updated')[0].last_updated
    except:
        latest_db_date = datetime.datetime.utcnow().replace(tzinfo=timezone.utc, year=datetime.MINYEAR)
    MiningObservation.objects.filter(last_updated=latest_db_date).delete() # purge last day so it can be updated
    observations_for_insert = []

    for observer_id in observer_db_list:
        observation_page = 1
        total_observation_pages = 1
        while observation_page <= total_observation_pages:
            ob_list, result = c.Industry.get_corporation_corporation_id_mining_observers_observer_id(
                corporation_id=_corporation.corporation_id,
                observer_id=observer_id,
                page=observation_page).result()
            observation_page = int(result.headers['X-Pages'])
            for observation in ob_list:
                last_updated_datetime = datetime.datetime(
                    observation.get('last_updated').year,
                    observation.get('last_updated').month,
                    observation.get('last_updated').day).replace(tzinfo=timezone.utc)
                if last_updated_datetime >= latest_db_date:
                    observations_for_insert.append(_observation_create(observation, last_updated_datetime, observer_id))

            observation_page += 1

    MiningObservation.objects.bulk_create(observations_for_insert, batch_size=500)


    return "Finished observers for: %s" % (str(character_id))


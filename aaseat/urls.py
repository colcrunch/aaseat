from django.conf.urls import url
from django.urls import path

from . import views

app_name = 'aaseat'

urlpatterns = [
    url(r'^$', views.aaseat_dashboard, name='dashboard'),
    url(r'^addchar/$', views.aaseat_add_char, name='add_char'),
    url(r'^addcorp/$', views.aaseat_dashboard, name='add_corp')
]
